#include <iostream>

using namespace std;

class Conversao {
private:
    char direcao;
    int bits;

public:
    Conversao(){};
    int converter(int base,int potBase,int item);
    int deslocar(int item);
    void setDirecao(char direcao);
    char getDirecao();
    int getBits();
    void setBits(int bits);
};

void Conversao::setBits(int bits) {
     this->bits = bits;
}

int Conversao::getBits() {
    return this->bits;
}

void Conversao::setDirecao(char direcao) {
    this->direcao = direcao;
}

char Conversao::getDirecao() {
    return this->direcao;
}

int Conversao::converter(int base,int potBase,int item){
        int resultado  = 0;
        int potencia = 1;

        while(item > 0) {
            resultado += item % base * potencia;
            potencia = potencia * potBase;
            item = item / base;
        }

        return resultado;
}

int Conversao::deslocar(int item){
    if(this->getDirecao() == 'd'){
        return item >> this->getBits();
    }else{
        if(this->getDirecao() == 'e'){
            return item << this->getBits();
        }
    }
};


int main() {

    char direcao;
    int op = NULL;
    int bits;
    int numero;
    
    Conversao co;
    
    cout<<"INFORME UMA OPERACAO\n";
    cout<<"1 - Deslocamento Decimal\n";
    cout<<"2 - Deslocamento Binario\n";
    cout<<"3 - Deslocamento Decimal E Deslocamento Binario\n";
    cout<<"4 - Decimal => Binario\n";
    cout<<"5 - Binario => Decimal\n";
    cin>>op;

    if(!op) return false;

    switch (op){

        case 1 :
            cout<<"Informe a direcao:";
            cin>>direcao;

            cout<<"Valor do conversao:";
            cin>>bits;

            cout<<"Informe o número decimal:";
            cin>>numero;

            co.setDirecao(direcao);
            co.setBits(bits);

            cout<<"Direção:"<<direcao<<"\n";
            cout<<"Deslocamento:"<<bits<<"\n";
            cout<<"Número:"<<numero<<"\n";
            cout<<"Resultado:"<< co.deslocar(numero);
            break;
        case 2 :
            cout<<"Informe a direcao:";
            cin>>direcao;

            cout<<"Valor do conversao:";
            cin>>bits;

            cout<<"Informe o número binario:";
            cin>>numero;

            co.setDirecao(direcao);
            co.setBits(bits);

            cout<<"\n\nDireção:"<<direcao<<"\n";
            cout<<"Deslocamento:"<<bits<<"\n";
            cout<<"Número:"<<numero<<"\n";
            cout<<"Resultado:"<< co.converter(2,10,co.deslocar(co.converter(10,2,numero)));
            break;
        case 3 :
            cout<<"Informe a direcao:";
            cin>>direcao;

            cout<<"Valor do conversao:";
            cin>>bits;

            cout<<"Informe o número:";
            cin>>numero;

            co.setDirecao(direcao);
            co.setBits(bits);

            cout<<"\n\nDireção:"<<direcao<<"\n";
            cout<<"Deslocamento:"<<bits<<"\n";
            cout<<"Número:"<<numero<<"\n";
            cout<<"Resultado em decimal:" << co.deslocar(co.converter(10,2,numero))<<"\n";
            cout<<"Resultado em binario:"<< co.converter(2,10,co.deslocar(co.converter(10,2,numero)));
            break;
        case 4 :
            cout<<"Informe o número decimal:";
            cin>>numero;
            cout<<"Resultado:"<<co.converter(2,10,numero);
            break;
        case 5 :
            cout<<"Informe o número binario:";
            cin>>numero;
            cout<<"Resultado:"<<co.converter(10,2,numero);
            break;
    }

   
}